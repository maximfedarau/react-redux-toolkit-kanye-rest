//RTK
import {
    configureStore,
    getDefaultMiddleware
} from "@reduxjs/toolkit"

//Root Reducer
import {
    rootReducer
} from "./root-reducer"

//Redux Logger
import logger from "redux-logger"

//Redux-Perist
import { 
    persistReducer,
    persistStore
 } from "redux-persist"
import storage from "redux-persist/lib/storage"

//Redux Thunk
import thunk from "redux-thunk"

const persistConfig = {
    key: 'root',
    storage,
    blacklist: [
        'quote'
    ]
}

const persistedReducer = persistReducer(persistConfig,rootReducer)

const middleware = [process.env.NODE_ENV !== "production" && logger].filter(Boolean)
const enhancers = [...middleware,thunk]

export const store = configureStore({
    reducer: persistedReducer,
    middleware: enhancers,
    devTools: false,
})

export const persistedStore = persistStore(store)