import {
    combineReducers
} from "@reduxjs/toolkit"

//Quote
import {
    quoteReducer
} from "./quote/quote.slice"

//Favourites
import {
    favouritesReducer
} from "./favourites/favourites.slice"

export const rootReducer = combineReducers({
    quote: quoteReducer,
    favourites: favouritesReducer
})