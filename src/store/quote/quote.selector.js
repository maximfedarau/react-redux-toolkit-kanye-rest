//RTK
import {
    createSelector
} from "@reduxjs/toolkit"

const quoteReducerSelector = (state) => state.quote

export const quoteSelector = createSelector(
    [quoteReducerSelector],
    (state) => state.quote 
)

export const isLoadingQuoteSelector = createSelector(
    [quoteReducerSelector],
    (state) => state.isLoading
)