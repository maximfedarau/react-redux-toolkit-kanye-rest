export const INITIAL_STATE = {
    quote: '',
    isLoading: false
}

export const QUOTE_ACTION_TYPES = {
    setQuote: "setQuote"
}