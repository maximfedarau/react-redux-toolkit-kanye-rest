//RTK
import {
    createSlice,
    createAsyncThunk
} from "@reduxjs/toolkit"

//Config
import {
    INITIAL_STATE,
    QUOTE_ACTION_TYPES
} from "./quote.config"

export const getQuote = createAsyncThunk(
    "quote/getQuote",
    async () => {
        const res = await fetch("https://api.kanye.rest/").then(data => data.json())
        return res.quote
    }
)

const quoteSlice = createSlice({
    name: 'quote',
    initialState: INITIAL_STATE,
    reducers: {
        [QUOTE_ACTION_TYPES.setQuote]: (state,action) => {
            state.quote = action.payload
        }
    },
    extraReducers: {
        [getQuote.pending]: (state) => {
            state.isLoading = true
        },
        [getQuote.fulfilled]: (state,action) => {
            state.quote = action.payload
            state.isLoading = false
        },
        [getQuote.rejected]: (state) => {
            state.isLoading = false
        }
    }
})

export const quoteReducer = quoteSlice.reducer

export const quoteActions = quoteSlice.actions

