export const INITIAL_STATE = {
    favourites: [],
}

export const FAVOURITES_ACTION_TYPES = {
    addQuote: "addQuote",
    removeQuote: "removeQuote"
}
