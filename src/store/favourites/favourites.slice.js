//RTK
import {
    createSlice
} from "@reduxjs/toolkit"

//config
import {
    INITIAL_STATE,
    FAVOURITES_ACTION_TYPES
} from "./favourites.config"

const favouritesSlice = createSlice({
    name: "favourites",
    initialState: INITIAL_STATE,
    reducers: {
        [FAVOURITES_ACTION_TYPES.addQuote] : (state,action) => {
            //find in array
            const found = state.favourites.find((element) => element === action.payload)
            if (!found) {
                state.favourites = state.favourites.concat([action.payload])
            } else {
                state.favourites = state.favourites.filter((quote) => {
                    return quote !== action.payload
                })
            }
        }
    }
})

export const favouritesActions = favouritesSlice.actions

export const favouritesReducer = favouritesSlice.reducer