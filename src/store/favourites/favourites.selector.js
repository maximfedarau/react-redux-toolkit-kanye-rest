//RTK
import {
    createSelector
} from "@reduxjs/toolkit"

const favouritesReducerSelector = (state) => state.favourites

export const favouritesSelector = createSelector(
    favouritesReducerSelector,
    (reducer) => reducer.favourites
)