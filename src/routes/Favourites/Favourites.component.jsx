//React
import React from "react"

//Chakra UI
import {
    Box
} from "@chakra-ui/react"

//React Redux
import {
    useSelector
} from "react-redux"

//Selectors
import {
    favouritesSelector
} from "../../store/favourites/favourites.selector"

//components
import FavouriteCard from "../../components/FavouriteCard/FavouriteCard.component"


export default function Favourites() {

    const favourites = useSelector(favouritesSelector)

    return (
        <Box>
            <Box>
            {favourites.map((quote) => {
                return <FavouriteCard key={quote} quote={quote} />
            })}
            </Box>
        </Box>
    )
}