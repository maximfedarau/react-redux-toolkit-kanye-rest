//React
import React from "react"

//Chakra UI
import {
    Box
} from "@chakra-ui/react"

//components
import QuotePanel from "../../components/QuotePanel/QuotePanel.component"

export default function Homepage() {
    return (
        <Box>
            <QuotePanel/>
        </Box>
    )
}