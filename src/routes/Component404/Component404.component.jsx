import React from "react"

import { Link } from "react-router-dom" 

import "./Component404.styles.scss"

export default class Component404 extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <div className="error-404-container">
                <h1 className="error-404">Oops! We cannot find this page.</h1>
                <Link to="/">
                    <button className="error-404-back-home-button">Back home</button>
                </Link>
            </div>
        )
    }
}