//React
import React from "react"

//React Router
import {
    Outlet,
    Link
} from "react-router-dom"

//CHakra UI
import {
    Box,
    Text,
    Button,
    HStack,
    Spacer
} from "@chakra-ui/react"

//Icons
import {
    IoHome,
    IoHeartSharp
} from "react-icons/io5"

export default function Header() {
    return (
        <Box>
            <Box width="100%" bg="black" color="white" textAlign="center" fontSize="25px" float="left" padding="10px">
                <HStack align="center" justify="center">
                <Text>Mr. Ye Quotes</Text><Spacer/>
                <HStack spacing="24px">
                <Link to="/">
                    <Button color="black" bg="white" _hover={{bg: "gray.400"}}><IoHome/></Button>
                </Link>
                <Link to="/favourites">
                    <Button color="red" bg="white" _hover={{bg: "gray.400"}}><IoHeartSharp/></Button>
                </Link>
                </HStack>
                </HStack>
            </Box>
            <Outlet/>
        </Box>
    )
}