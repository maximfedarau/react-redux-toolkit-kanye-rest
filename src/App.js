import React from "react"

//Chakra UI
import {
  ChakraProvider
} from "@chakra-ui/react"

//React-Router
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom"

//Redux
import {
  Provider
} from "react-redux"
import {
  store
} from "./store/store"

//Components

//For pages
import Header from "./routes/Header/Header.component"
import Homepage from "./routes/Homepage/Homepage.component"
import Favourites from "./routes/Favourites/Favourites.component"
import Component404 from "./routes/Component404/Component404.component"

//Redux Persis
import { PersistGate } from "redux-persist/integration/react"
import {persistedStore} from "./store/store"


function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistedStore}>
      <ChakraProvider>
        <BrowserRouter>
            <Routes>
              <Route path="/" element={<Header/>}>
                <Route index element={<Homepage/>} />
                <Route path="/favourites" element={<Favourites/>} />
              </Route>
              <Route path="/*" element={<Component404/>} />
            </Routes>
        </BrowserRouter>
      </ChakraProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
