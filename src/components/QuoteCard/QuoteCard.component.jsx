//React Redux
import {
    useDispatch,
    useSelector
} from "react-redux"

//Actions
import {
    favouritesActions
} from "../../store/favourites/favourites.slice"

//Action types
import {
    FAVOURITES_ACTION_TYPES
} from "../../store/favourites/favourites.config"

//Selectors
import {
    favouritesSelector
} from "../../store/favourites/favourites.selector"

//Chakra UI
import {
    Box,
    Text,
    Button
} from "@chakra-ui/react"

//icons
import {
    IoHeartCircleSharp,
    IoHeartDislikeCircleSharp
} from "react-icons/io5"


export default function QuoteCard({quote}) {
    
    const dispatch = useDispatch()

    const addQuote = favouritesActions[FAVOURITES_ACTION_TYPES.addQuote]

    const favourites = useSelector(favouritesSelector)

    function onClickAddQuoteHandler() {
        dispatch(addQuote(quote))
    }

    return (
        <Box border="1px solid" borderRadius="1rem" padding="10px">
            <Text>{quote}</Text>
            <Button color="red" fontSize="25px" onClick={onClickAddQuoteHandler}>{favourites.find((element) => element === quote) ? <IoHeartDislikeCircleSharp/> : <IoHeartCircleSharp/>}</Button>
        </Box>
    )
}