//React
import React from "react"

//Chakra UI
import { 
    Center,
    Text,
    Button,
    Spacer
} from "@chakra-ui/react"

//React Redux
import {
    useDispatch
} from "react-redux"

//Actions
import {
    favouritesActions
} from "../../store/favourites/favourites.slice"

//Action types
import {
    FAVOURITES_ACTION_TYPES
} from "../../store/favourites/favourites.config"

export default function FavouriteCard({quote}) {

    const dispatch = useDispatch()

    const removeQuote = favouritesActions[FAVOURITES_ACTION_TYPES.addQuote]

    const [showRemoveButton,setShowRemoveButton] = React.useState(false)

    function onMouseOverShowRemoveButtonHandler() {
        setShowRemoveButton(true)
    }

    function onMouseLeaveShowRemoveButtonHandler() {
        setShowRemoveButton(false)
    }

    function onClickRemoveHandler() {
        dispatch(removeQuote(quote))
    }

    return (
        <Center
        border="1px solid"
        borderRadius="1rem"
        marginTop="20px"
        padding="10px"
        onMouseOutCapture={onMouseOverShowRemoveButtonHandler}
        onMouseLeave={onMouseLeaveShowRemoveButtonHandler}>
            <Text>{quote}</Text>
            <Spacer/>
            {showRemoveButton && <Button onClick={onClickRemoveHandler}>x</Button>}
        </Center>
    )
}