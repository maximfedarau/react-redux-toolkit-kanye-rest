//React
import React from "react"

//React Redux
import {
    useSelector,
    useDispatch
} from "react-redux"

//Redux Actions
import {
    getQuote
} from "../../store/quote/quote.slice"

//Redux Selectors
import  {
    quoteSelector,
    isLoadingQuoteSelector
} from "../../store/quote/quote.selector"

//Chakra UI
import {
    Box,
    Button,
    Text,
    Image,
    Spinner
} from "@chakra-ui/react"

//components
import QuoteCard from "../QuoteCard/QuoteCard.component"

export default function QuotePanel() {

    const dispatch = useDispatch()

    const quote = useSelector(quoteSelector)
    const isLoading = useSelector(isLoadingQuoteSelector)

    React.useEffect(() => {
        dispatch(getQuote())
    },[dispatch])

    function onClickGetQuoteHandler() {
        dispatch(getQuote())
    }

    return (
        <Box textAlign="center" alignContent="center">
            <Box align="center">
                <Image src="https://freepngimg.com/download/kanye_west/8-2-kanye-west-picture.png"/>
            </Box>
            {isLoading ? <Spinner/> : 
            <Box>
                <QuoteCard quote={quote} />
                <Button onClick={onClickGetQuoteHandler} marginTop="10px">Get Quote</Button>    
            </Box>}
        </Box>
    )
}